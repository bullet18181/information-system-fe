import 'babel-polyfill'
import Vue from 'vue'
import Axios from 'axios'
import Cookie from 'vue-cookies'
import App from './App.vue'
import store from './store'
import router from './router'
import Mixins from './mixins/general'
import vuetify from './plugins/vuetify'

let headers = {}
let token = Cookie.get('token')
let url = 'http://si.local/api/'

if ( token ) {
    headers = { 'token': `${token}`}
}

let axiosInstance = Axios.create({
    baseURL: url,
    headers: { 'token': `${token}`}
})

let documentManager = Axios.create({ baseURL: 'http://si.local/' })

axiosInstance.interceptors.response.use(
    ( response ) => {
        return response
    },
    ( error ) => {
        if ( token && error.response.status == 401 ) {
            localStorage.removeItem('siis_data')
            Cookie.remove('token')
            location.reload()
        }
        return error.response
    }
)

documentManager.interceptors.response.use( (response) => { return response } )

// window.$_ = _
window.axios = axiosInstance
window.docman = documentManager

Vue.config.productionTip = false

Vue.mixin(Mixins)

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
