import { getField, updateField } from 'vuex-map-fields'

const state = {
	employee_list: [
		{
			value: 1,
			text: 'Mel Reyes'
		},
		{
			value: 2,
			text: 'Patrick De Jesus'
		},
		{
			value: 3,
			text: 'Gilber Tuazon'
		}
	],
	level_list: ['Junior', 'Mid', 'Senior'],
	job_title_list: ['Developer', 'Systems Analyst', 'Quality Assurance']
}

const getters = {
    getField
}

const actions = {
    
}

const mutations = {
    updateField
}

export const employee = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}