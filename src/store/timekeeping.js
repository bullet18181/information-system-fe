import api from '../api/timekeeping'
import { getField, updateField } from 'vuex-map-fields'

const state = {
    logs: [],
    message: null,
    
    index: null,
    sub_index: null,

    log_id: null,
    user_id: null,
    work_date: null,
    time_in: null,
    time_out: null,

    /*Dialogs */
    search_dialog: false,
    add_log_dialog: false,
    update_log_dialog: false,

    /* Search */
    date_from: null,
    date_to: null,
    keyword: null,
    level: null,
    job_title: null

}

const getters = {
    getField
}

const actions = {
    async getLogs({commit}, params = {}) {
        let response = await api.getLogs(params)
        if (response.status == 200) {
            commit('onOkGetLogs', response.data)
        }
    },
    async exportLogs({commit}, params = {}) {
        let response = await api.exportLogs(params)
        if (response.status == 200) {
            commit('onOkExportLogs', response.data)
        }
        return response
    },
    async addLog({commit}, params = {}) {
        let response = await api.addLog(params)
        if (response.status == 200) {
            commit('onOkAddLog', response.data)
        }
    },
    async updateLog({commit}, params = {}) {
        let response = await api.updateLog(params)
        if (response.status == 200) {
            commit('onOkUpdateLog', { params, response })
        }
    }
}

const mutations = {
    updateField,
    onOkGetLogs(state, data) {
        state.logs = data.data
    },
    onOkExportLogs(state, data) {
        state.csv_path = data
    },
    onOkAddLog(state, data) {

    },
    onOkUpdateLog(state, data) {
        let index = data.params['index']
        let sub_index = data.params['sub_index']
        let new_data = data.response.data.data[0]

        state.logs[index]['logs'][sub_index]['time_out'] = new_data['time_out']
        state.logs[index]['logs'][sub_index]['total_log_hours'] = new_data['total_log_hours']
        state.logs[index]['logs'][sub_index]['total_working_hours'] = new_data['total_working_hours']
    }
}

export const timekeeping = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}