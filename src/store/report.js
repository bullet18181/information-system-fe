import { getField, updateField } from 'vuex-map-fields'

const state = {

}

const getters = {
    getField
}

const actions = {
    
}

const mutations = {
    updateField
}

export const report = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}