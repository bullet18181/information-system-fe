import { getField, updateField } from 'vuex-map-fields'
import api from '../api/form'

const state = {
	form_records: [],
}

const getters = {
    getField
}

const actions = {
    async getAllFormRecords({commit}){
    	let res = await api.getAllRecords()
        if (res.status === 200){
            commit('onOkGetAllFormRecords', res.data)
        }
    },

    async createRequestForm({commit}, body = {}){
        let res = await api.createRequestForm(body)
        return res
    },

    
}

const mutations = {
    updateField,
    //Get All Records
    onOkGetAllFormRecords(state, data) {
        state.form_records = data.data
    },

    clearFormRecords(state, data){
    	state.form_records = []
    },
}

export const form = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}