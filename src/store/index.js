import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'

import { announcement } from './announcement.js'
import { auth } from './auth.js'
import { conference } from './conference.js'
import { dashboard } from './dashboard.js'
import { document } from './document.js'
import { employee } from './employee.js'
import { form } from './form.js'
import { report } from './report.js'
import { task } from './task.js'
import { timekeeping } from './timekeeping.js'
import { utils } from './utils.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	namespaced: true,
	modules: {
        announcement,
        auth,
        conference,
        dashboard,     
        document,
        employee,
        form,
        report,
        task,
        timekeeping,
        utils
	},
	state:{
	
	},
	getters: {
		getField,
	},
	actions: {
	
	},
	mutations: {
		updateField,
	}
})

export default store