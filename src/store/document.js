import { getField, updateField } from 'vuex-map-fields'
import api from '../api/document'

const state = {

}

const getters = {
    getField
}

const actions = {
    getFileManager({commit}) {
        return api.getFileManager()
    }
}

const mutations = {
    updateField
}

export const document = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}