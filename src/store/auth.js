import { getField, updateField } from 'vuex-map-fields'
// import Cookie from 'vue-cookies'

const state = {

}

const getters = {
    getField
}

const actions = {
    async getAuth({commit, state}) {
        window.localStorage.setItem('userdata', JSON.stringify({id: 1, username: 'Admin', status: 'active'}))
        window.location.reload()
        return await true
    },
    async destroyAuth({commit, state}) {
        window.localStorage.removeItem('userdata')
        window.location.reload()
    }
}

const mutations = {
    updateField
}

export const auth = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}