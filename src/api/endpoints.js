export const endpoint = {
    general: {
        get_api_version: 'version'
    },
    announcement: {
        get_announcements: '',
        get_announcement: '',
        delete_announcement: '',
        update_announcement: '',
        add_announcement: '',
    },
    auth: {},
    conference: {},
    dashboard: {},
    document_manager: {
        file_manager: 'file-manager/fm-button'
    },
    employee: {},
    form: {
        get_form_records: 'form-management/request/',
        add_form_request: 'form-management/request/add'
    },
    report: {},
    task: {},
    timekeeping: {
        get_logs: 'timekeeping/',
        export_logs: 'timekeeping/export',
        add_log: 'timekeeping/add',
        update_log: 'timekeeping/update/',
    }
}