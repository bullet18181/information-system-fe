import { endpoint } from './endpoints'

export default {
    getFileManager() {
        return endpoint.document_manager.file_manager
    }
}