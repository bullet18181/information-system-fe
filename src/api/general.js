import { endpoint } from './endpoints'

export default {
    async getAppVersion() {
        return await axios.get(endpoint.general.get_api_version)
    }
}