import { endpoint } from './endpoints'

export default {
	async getAllRecords(){
		return await axios.get(endpoint.form.get_form_records)
	},

	async createRequestForm(body){
		return await axios.post(endpoint.form.add_form_request,body)
	},
}