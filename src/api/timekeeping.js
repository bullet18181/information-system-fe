import { endpoint } from './endpoints'

export default {
    async getLogs(params) {
        return await axios.post(
            endpoint.timekeeping.get_logs,
            {
                date_from: params.date_from,
                date_to: params.date_to,
                keyword: params.keyword,
                level: params.level,
                job_title: params.job_title,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
        )
    },
    async exportLogs(params) {
        return await axios.post(
            endpoint.timekeeping.export_logs,
            {
                date_from: params.date_from,
                date_to: params.date_to,
                keyword: params.keyword,
                level: params.level,
                job_title: params.job_title,
                user_id: params.user_id,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
        )
    },
    async addLog(params) {
        return await axios.post(
            endpoint.timekeeping.add_log,
            {
                user_id: params.user_id,
                work_date: params.work_date,
                time_in: params.time_in,
                time_out: params.time_out,
                header: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
        )
    },
    async updateLog(params) {
        return await axios.put(
            endpoint.timekeeping.update_log,
            {
                id: params.id,
                time_out: params.time_out,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
        )
    }
}