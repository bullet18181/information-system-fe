import { mapFields } from 'vuex-map-fields'
import { mapMutations } from 'vuex'

import ConfirmationBox from '../components/base/ConfirmModal'
import DateTimePicker from '../components/base/DateTimePicker'

export default {
    name: 'GeneralMixins',
    components: {
        ConfirmationBox,
        DateTimePicker
    },
    methods: {
        ...mapMutations('utils', [
            'togglePageLoader',
            'toggleQuickMessage'
        ]),
        toggleLoader() {
            this.togglePageLoader()
        },
        toggleSnackbar(msg = {}) {
            this.toggleQuickMessage(msg)
        }
    },

    
}