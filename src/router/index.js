import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/HelloWorld'
import Dashboard from '../views/Dashboard/MainDashboard'
import Announcement from '../views/Announcement/Announcement'
import Auth from '../views/Auth/Login'
import RequestForms from '../views/Forms/RequestForms'
import Utilities from '../views/Utilities/Utilities'
import Reports from '../views/Report/Report'
import Conference from '../views/Conference/Conference'
import Settings from '../views/Settings/Settings'
import Timekeeping from '../views/Timekeeping/Timekeeping'
import DocumentManagement from '../views/DocumentManagement/DocumentManagement'
import MeetTheDevs from '../views/AboutUs/MeetTheDevs'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            title: 'Dashboard', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Dashboard' },
        },
        {
            path: '/auth',
            name: 'auth',
            component: Auth,
            title: 'Auth', icon: '', visible: false,
            meta: { requires_auth: false, title: 'Auth' }
        },
        {
            path: '/announcement',
            name: 'announcement',
            component: Announcement,
            title: 'Announcement', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Announcement' }
        },
        {
            path: '/hr-forms',
            name: 'hr-forms',
            component: RequestForms,
            title: 'HR Forms', icon: '', visible: true,
            meta: { requires_auth: true, title: 'HR Forms' }
        },
        {
            path: '/utilities',
            name: 'utilities',
            component: Utilities,
            title: 'Utilities', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Utilities' }
        },
        {
            path: '/reports',
            name: 'reports',
            component: Reports,
            title: 'Reports', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Reports' }
        },
        {
            path: '/conference',
            name: 'conference',
            component: Conference,
            title: 'Conference', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Conference' }
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings,
            title: 'Settings', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Settings' }
        },
        {
            path: '/timekeeping',
            name: 'timekeeping',
            component: Timekeeping,
            title: 'Timekeeping', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Timekeeping' }
        },
        {
            path: '/document-management',
            name: 'document-management',
            component: DocumentManagement,
            title: 'Document Management', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Document Management' }
        },
        {
            path: '/meet-the-devs',
            name: 'meet-the-devs',
            component: MeetTheDevs,
            title: 'Meet the Developers', icon: '', visible: true,
            meta: { requires_auth: true, title: 'Meet the Developers' }
        }
    ]
})

router.beforeEach((to, from, next) => {
    // Restrict access to protected routes if not logged in
    if (to.meta.requires_auth && !window.localStorage.getItem('userdata')) {
        //TODO: you can add api request here to check if Authorization token is valid
    	next({name: 'auth'})
		return
    }
    if (!to.meta.requires_auth && window.localStorage.getItem('userdata')) {
		next({name: 'dashboard'})
		return
	}
    // if (to.meta.not_for_authenticated && axios.defaults.headers.Authorization.indexOf('null') < 0) {
	// 	next({name: 'dashboard'})
	// 	return
	// }
    // Restrict access to Login page if user is logged in
    // else  {
    //     next({name: 'auth'})
    //     return
    // }
    next()
})

export default router