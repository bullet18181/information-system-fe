import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        themes: {
            light: {
                primary: '#04bad2', //5da7e2
                secondary: '#57585a',
                success: '#76b936  ', //65c769
                info: '#2da2ff',
                warning: '#f4a735', //f4a735
                accent: '#ff6c00',
                error: '#f34334', //f1322f
                gray1: '#43454d',
                gray2: '#3a3c44',
                gray3: '#3c3e47',
                gray4: '#33363f',
                gray5: '#2a2d34',
                gray6: '#24262c',
                gold1: '#F5AE4A',
                gold2: '#F4A735',
                gold3: '#DC9440',
            },
            dark: {
                primary: colors.blue.lighten3,
            },
        },
    }
});
